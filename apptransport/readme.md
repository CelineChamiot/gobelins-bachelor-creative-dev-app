# Application transport 

Uzume app est une application qui permet de faire évoluer des personnages en fonction du temps passer dans les transports en commun. 

## Concept 

L'utilisateur doit scanner le qrcode qui se trouve à son arret de bus et à l'arrivé pour gagner de l'argent et pouvoir acheter un nouveau perso. 

## Intallation 

    cd build 
    yarn
    cd ..
    cordova plugin add cordova-plugin-qrscanner
    cordova plugin add cordova-plugin-whitelist
    cordova platform add android
    cordova run android
    cordova build android

## Qr Code pour le départ du bus

![Départ](http://img15.hostingpics.net/pics/367096dpart2.png)

http://img15.hostingpics.net/pics/367096dpart2.png

##Qr Code pour l'arrivée du bus

![Arrivée](http://img15.hostingpics.net/pics/425291ariv.png)

http://img15.hostingpics.net/pics/425291ariv.png

## Info 

Sur la dernière version de cordova, j'ai des problèmes d'icone qui se déplacent pas dans platforms/android/res, j'ai du les copier à la main pour que ça marche.

## APK Finale 

build > apk

CHAMIOT-PONCET Céline