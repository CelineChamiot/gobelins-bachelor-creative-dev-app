var gulp = require('gulp');
var sass = require('gulp-sass');

//convert scss files into css file
gulp.task('sass', function () {
    return gulp.src('scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('../www/css/'))
});

//watch file .scss
gulp.task('watch', function () {
    gulp.watch('scss/**/*.scss',                 ['sass']);
});

// Defult task
gulp.task('default', ['sass', 'watch']);
