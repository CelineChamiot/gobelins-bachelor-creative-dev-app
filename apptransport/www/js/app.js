var app = (function() {

    /****************
     * Object App
     */
    var App = {

        init: function(){
            console.log("Home init")
            this.audio();
            this.home();
            money.init();
            this.initListener();

        },
        audio : function(){
            var audio = document.getElementById("Ambient");
            audio.volume = 0.2;
        },
        slideChanging : function(slideFirst, slideSecond){

            slideFirst.addClass('Page-hidden');
            slideFirst.removeClass('Page-active');

            slideSecond.addClass('Page-active');
            slideSecond.removeClass('Page-hidden');

        },
        home : function(){
            console.log("Home component");
            var homeDom = $(".Home__container h1");
            TweenMax.to(homeDom, 1, {width: 240, ease: Power4.easeInOut}, 500);
            TweenMax.to(homeDom, 3, {alpha: 0, ease: Power4.easeInOut, onComplete:this.slideChanging, onCompleteParams:[$(".Home"), $(".Animal")]}, 600);
            this.animal();
        },
        animal : function(){
            console.log("Animal Component");

            $( ".Animal__menuIcon" ).on( "click", function() {
                $(".Animal__containerItem").toggleClass("Animal__containerItem-active");
            });

            $( ".Animal__qrcode" ).on( "click", function() {
                qrcode.askForPermission();
                $( ".Page" ).addClass("Page-none");
                $( ".Overlay" ).addClass("Overlay-active");
            });
        },
        initListener: function(){
            $( ".Animal__item" ).on( "click", function() {
                if(money.getMoney()  >= 250){
                    console.log($(this).data("name"));
                    money.achatItem($(this), $(this).data("name"));
                }
            });
        }
    };

    return App;
})();

document.addEventListener("deviceready", onDeviceReady.bind(this), false);

function onDeviceReady() {
    app.init();
}