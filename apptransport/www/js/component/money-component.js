var money = (function() {

    /****************
     * Object Money
     */
    var Money = {

        currentMoney: null,

        init: function() {
            this.findLastMoney();
            this.replaceMoney();
            console.log("money component");
        },
        replaceMoney: function() {
            $('.Animal__money').html(this.currentMoney+" $");
        },
        addMoney: function(number) {
            this.currentMoney =  parseInt(this.currentMoney) + number;
            localStorage.setItem('Money', this.currentMoney);
            this.replaceMoney();
        },
        findLastMoney: function(){

            if(localStorage.getItem('Money') == null){
                this.currentMoney = 0;
            } else {
                this.currentMoney = localStorage.getItem('Money');
            }
        },
        getMoney: function(){
            return this.currentMoney;
        },
        achatItem: function(item, itemImagePath){
            item.addClass("Animal__item-checked");
            $(".Animal__img").attr("src", "img/"+itemImagePath+".svg");
            this.currentMoney = parseInt(this.currentMoney) - 250;
            localStorage.setItem('Money', this.currentMoney);
            this.replaceMoney();
        }
    };

    return Money;
})();

