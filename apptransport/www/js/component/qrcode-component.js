var qrcode = (function() {

    /****************
     * Object Qrcode
     */
    var Qrcode = {

        table: null,
        time: null,

        askForPermission: function(){
            this.table = [];
            QRScanner.prepare(this.onDone);
        },
        onDone : function(error, status){
            if (error) {
                console.error(message);
            }
            if (status.authorized) {
                QRScanner.scan(Qrcode.displayContents);
                QRScanner.show();

                console.log("autorised")
            } else if (status.denied) {
                console.log("refused")
            }
        },
        displayContents: function(err, text){
            if(err){
                console.log(err);
                console.log("error");
            } else {
                console.log(text);
                Qrcode.addBusStation(text);
                $( ".Page" ).removeClass("Page-none");
                $( ".Overlay" ).removeClass("Overlay-active");
            }

            //Destuction du scanner
            QRScanner.destroy();
        },
        addBusStation: function(text) {

            var d = new Date();
            var seconds = d.getSeconds();
            var minutes = d.getMinutes();
            var hour = d.getHours();
            this.time = hour+minutes+seconds;

            if(text == "départ" && this.table[2] == null){
                this.table[1] = this.time;
                console.log("départ "+ this.table[1]);
            }

            if(text == "arrivé"){
                this.table[2] = this.time;
                console.log("arrivé "+ this.table[2]);

                money.addMoney(250);
                this.cleanUp();
            }
        },
        cleanUp: function(){
            this.table[1] = null;
            this.table[2] = null;
        }
    };

    return Qrcode;
})();

